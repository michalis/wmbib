define(BIB_APPENDIX_TITLE, ifdef(`APPENDIX_TITLE', APPENDIX_TITLE, Supplementary material for this paper.))dnl
define(BIB_APPENDIX_AUTHOR, ifdef(`APPENDIX_AUTHOR', APPENDIX_AUTHOR, Supplementary material))dnl
define(BIB_ANONYMIZED_TITLE, ifdef(`ANONYMIZED_TITLE', ANONYMIZED_TITLE, Anonymized material for this submission.))dnl
define(BIB_ANONYMIZED_AUTHOR, ifdef(`ANONYMIZED_AUTHOR', ANONYMIZED_AUTHOR, Anonymous author(s)))dnl
define(BIB_CURRENT_YEAR, ifdef(`CURRENT_YEAR', CURRENT_YEAR, 2022))dnl
dnl
define(CONF_ABZ, ifdef(`FULL', ``Abstract State Machines, B and Z'', ABZ))dnl
define(CONF_ACSD, ifdef(`FULL', International Conference on Application of Concurrency to System Design, ACSD))dnl
define(CONF_APLAS, ifdef(`FULL', Asian Symposium on Programming Languages and Systems, APLAS))dnl
define(CONF_APSYS, ifdef(`FULL', Asia Pacific Workshop on Systems, APSys))dnl
define(CONF_ASE, ifdef(`FULL', International Conference on Automated Software Engineering, ASE))dnl
define(CONF_ASPLOS, ifdef(`FULL', International Conference on Architectural Support For Programming Languages And Operating Systems, ASPLOS))dnl
define(CONF_ATVA, ifdef(`FULL', International Symposium on Automated Technology for Verification and Analysis, ATVA))dnl
define(CONF_CAV, ifdef(`FULL', International Conference on Computer Aided Verification, CAV))dnl
define(CONF_CC, ifdef(`FULL', International Conference on Compiler Construction, CC))dnl
define(CONF_CGO, ifdef(`FULL', International Symposium on Code Generation and Optimization, CGO))dnl
define(CONF_CONCUR, ifdef(`FULL', International Conference on Concurrency Theory, CONCUR))dnl
define(CONF_CSFS, ifdef(`FULL', Computer Security Foundations Symposium, CSFS))dnl
define(CONF_CSL, ifdef(`FULL', International Workshop on Computer Science Logic, CSL))dnl
define(CONF_DAC, ifdef(`FULL', ``Design Automation Conference'', DAC))dnl
define(CONF_DATE, ifdef(`FULL', ``Design, Automation, and Test in Europe'', DATE))dnl
define(CONF_DFRWS, ifdef(`FULL', Digital Forensic Research Workshop, DFRWS))dnl
define(CONF_DISC, ifdef(`FULL', International Symposium on Distributed Computing, DISC))dnl
define(CONF_EC2, ifdef(`FULL', International Workshop on Exploiting Concurrency Efficiently and Correctly, EC{$^2$}))dnl
define(CONF_ECOOP, ifdef(`FULL', European Conference on Object-Oriented Programming, ECOOP))dnl
define(CONF_ERLANG, ifdef(`FULL', {ACM} {SIGPLAN} Erlang Workshop, Erlang 2022))dnl
define(CONF_ESOP, ifdef(`FULL', European Symposium on Programming, ESOP))dnl
define(CONF_EUROPAR, ifdef(`FULL', European Conference on Parallel and Distributed Computing, Euro-Par))dnl
define(CONF_EUROSYS, ifdef(`FULL', European Conference on Computer Systems, EuroSys))dnl
define(CONF_FASE, ifdef(`FULL', Fundamental Approaches to Software Engineering, FASE))dnl
define(CONF_FAST, ifdef(`FULL', USENIX Conference on File and Storage Technologies, FAST))dnl
define(CONF_FM, ifdef(`FULL', International Symposium on Formal Methods, FM))dnl
define(CONF_FMCAD, ifdef(`FULL', International Conference on Formal Methods in Computer-Aided Design, FMCAD))dnl
define(CONF_FMCO, ifdef(`FULL', International Conference on Formal Methods for Components and Objects, FMCO))dnl
define(CONF_FPS, ifdef(`FULL', From Programs to Systems. The Systems perspective in Computing, FPS))dnl
define(CONF_FORTE, ifdef(`FULL', ``International Conference on Formal Techniques for Distributed Objects, Components, and Systems'', FORTE))dnl
define(CONF_FREENIX, ifdef(`FULL', USENIX Annual Technical Conference (FREENIX Track), USENIX ATC (FREENIX Track)))dnl
define(CONF_HVC, ifdef(`FULL', International Haifa Verification Conference, HVC))dnl
define(CONF_ICALP, ifdef(`FULL', ``International Colloquium on Automata, Languages and Programming'', ICALP))dnl
define(CONF_ICCL, ifdef(`FULL', International Conference on Computer Languages, ICCL))dnl
define(CONF_ICDCS, ifdef(`FULL', International Conference on Distributed Computing Systems, ICDCS))dnl
define(CONF_ICFP, ifdef(`FULL', International Conference on Functional Programming, ICFP))dnl
define(CONF_IFIP, ifdef(`FULL', Information Processing, IFIP))dnl
define(CONF_IFM, ifdef(`FULL', International Conference on integrated Formal Methods, IFM))dnl
define(CONF_ISCA, ifdef(`FULL', International Symposium on Computer Architecture, ISCA))dnl
define(CONF_ISP, ifdef(`FULL', International Symposium on Programming, ISP))dnl
define(CONF_ISSTA, ifdef(`FULL', International Symposium on Software Testing and Analysis, ISSTA))dnl
define(CONF_ITP, ifdef(`FULL', International Conference on Interactive Theorem Proving, ITP))dnl
define(CONF_JMLC, ifdef(`FULL', Joint Modular Languages Conference, JMLC))dnl
define(CONF_LICS, ifdef(`FULL', Symposium on Logic in Computer Science, LICS))dnl
define(CONF_LINUXEXPO, ifdef(`FULL', LinuxExpo, LinuxExpo))dnl
define(CONF_LOLA, ifdef(`FULL', Syntax and Semantics of Low-Level Languages, LOLA))dnl
define(CONF_MSPC, ifdef(`FULL', Workshop on Memory Systems Performance and Correctness, MSPC))dnl
define(CONF_NETYS, ifdef(`FULL', International Conference on Networked Systems, NETYS))dnl
define(CONF_NSDI, ifdef(`FULL', USENIX Conference on Networked Systems Design \& Implementation, NSDI))dnl
define(CONF_OLS, ifdef(`FULL', Ottawa Linux Symposium, OLS))dnl
define(CONF_OOPSLA, ifdef(`FULL', ``Conference on Object-Oriented Programming Systems, Languages,and Applications'', OOPSLA))dnl
define(CONF_OPODIS, ifdef(`FULL', International Conference on Principles of Distributed Systems, OPODIS))dnl
define(CONF_OSDI, ifdef(`FULL', USENIX Symposium on Operating Systems Design and Implementation, OSDI))dnl
define(CONF_PLDI, ifdef(`FULL', Programming Language Design and Implementation, PLDI))dnl
define(CONF_PLOS, ifdef(`FULL', Workshop on Programming Languages and Operating Systems, PLOS))dnl
define(CONF_PLS, ifdef(`FULL', International Conference on PLS and Related Methods, PLS))dnl
define(CONF_PNAROMC, ifdef(`FULL', Petri Nets: Applications and Relationships to Other Models of Concurrency, PNAROMC))dnl
define(CONF_PODC, ifdef(`FULL', Principles of Distributed Computing, PODC))dnl
define(CONF_POPL, ifdef(`FULL', Symposium on Principles of Programming Languages, POPL))dnl
define(CONF_POST, ifdef(`FULL', Conference on Principles of Security and Trust, POST))dnl
define(CONF_PPDP, ifdef(`FULL', International Symposium on Principles and Practice of Declarative Programming, PPDP))dnl
define(CONF_PPOPP, ifdef(`FULL', Principles and Practice of Parallel Programming, PPoPP))dnl
define(CONF_RAMICS, ifdef(`FULL', International Conference on Relational and Algebraic Methods in Computer Science, RAMiCS))dnl
define(CONF_RV, ifdef(`FULL', International Conference on Runtime Verification, RV))dnl
define(CONF_SAS, ifdef(`FULL', Static Analysis Symposium, SAS))dnl
define(CONF_SOCC, ifdef(`FULL', Symposium on Cloud Computing, SoCC))dnl
define(CONF_SOSP, ifdef(`FULL', Symposium on Operating Systems Principles, SOSP))dnl
define(CONF_SPAA, ifdef(`FULL', Symposium on Parallelism in Algorithms and Architectures, SPAA))dnl
define(CONF_SP, ifdef(`FULL', Symposium on Security and Privacy, SP))dnl
define(CONF_SPIN, ifdef(`FULL', International SPIN Symposium on Model Checking of Software, SPIN))dnl
define(CONF_TACAS, ifdef(`FULL', International Conference on Tools and Algorithms for the Construction and Analysis of Systems, TACAS))dnl
define(CONF_TAP, ifdef(`FULL', International Conference on Tests and Proofs, TAP))dnl
define(CONF_TASE, ifdef(`FULL', Theoretical Aspects of Software Engineering, TASE))dnl
define(CONF_TFP, ifdef(`FULL', International Symposium on Trends in Functional Programming 2012, TFP))dnl
define(CONF_TLCA, ifdef(`FULL', Conference on Typed Lambda Calculi and Applications, TLCA))dnl
define(CONF_TLDI, ifdef(`FULL', Workshop on Types in Language Design and Implementation, TLDI))dnl
define(CONF_TPHOLS, ifdef(`FULL', International Conference on Theorem Proving in Higher Order Logics, TPHOLs))dnl
define(CONF_USENIX, ifdef(`FULL', Usenix Annual Technical Conference, USENIX ATC))dnl
define(CONF_VMCAI, ifdef(`FULL', ``International Conference on Verification, Model Checking, and Abstract Interpretation'', VMCAI))dnl
define(CONF_VSTTE, ifdef(`FULL', ``Verified Software: Theories, Tools and Experiments'', VSTTE))dnl
define(CONF_WPTE, ifdef(`FULL', International Workshop on Rewriting Techniques for Program Transformations and Evaluation, WPTE))dnl
dnl
define(JNL_ACMCS, ifdef(`FULL', ACM Computing Surveys, {ACM} Comput. Surv.))dnl
define(JNL_ACMTCS, ifdef(`FULL', ACM Transactions on Computer Systems, {ACM} Trans. Comput. Syst.))dnl
define(JNL_AI, ifdef(`FULL', Acta Informatica, Act. Inform.))dnl
define(JNL_ASE, ifdef(`FULL', Automated Software Engineering, Autom. Softw. Eng.))dnl
define(JNL_CAN, ifdef(`FULL', ACM SIGARCH Computer Architecture News, {SIGARCH} Comput. Archit. News))dnl
define(JNL_CACM, ifdef(`FULL', Communications of the ACM, Commun. {ACM}))dnl
define(JNL_CCPE, ifdef(`FULL', Concurrency and Computation: Practice and Experience, Concurr. Comput. Pract. Exp.))dnl
define(JNL_COMPUTER, ifdef(`FULL', IEEE Computer, {IEEE} Comput.))dnl
define(JNL_CORR, ifdef(`FULL', Computing Research Repository, {CoRR}))dnl
define(JNL_DC, ifdef(`FULL', Distributed Computing, Distrib. Comput.))dnl
define(JNL_ENTCS, ifdef(`FULL', Electronic Notes in Theoretical Computer Science, Elect. Not. Theor. Comput. Sci.))dnl
define(JNL_FAC, ifdef(`FULL', Formal Aspects of Computing, Formal Asp. Comput.))dnl
define(JNL_FMSD, ifdef(`FULL', Formal Methods in System Design, Form. Meth. Syst. Des.))dnl
define(JNL_HAL, ifdef(`FULL', Archive ouverte HAL, {HAL}))dnl
define(JNL_JACM, ifdef(`FULL', Journal of the ACM, J. {ACM}))dnl
define(JNL_JCOMP, ifdef(`FULL', SIAM Journal on Computing, {SIAM} J. Comput.))dnl
define(JNL_JFP, ifdef(`FULL', Journal of Functional Programming, J. {FP}))dnl
define(JNL_JPDC, ifdef(`FULL', Journal of Parallel and Distributed Computing, J. Parallel Distrib. Comput.))dnl
define(JNL_MOS, ifdef(`FULL', Mathematics of Computation, Math. Comput.))dnl
define(JNL_PACMPL, ifdef(`FULL', Proceedings of the ACM on Programming Languages, Proc. ACM Program. Lang.))dnl
define(JNL_SCP, ifdef(`FULL', Science of Computer Programming, Sci. Comput. Program.))dnl
define(JNL_SIGACTNEWS, ifdef(`FULL', {SIGACT} News, {SIGACT} News))dnl
define(JNL_SN, ifdef(`FULL', {SIGPLAN} Notices, {SIGPLAN} Not.))dnl
define(JNL_SEN, ifdef(`FULL', {ACM} {SIGSOFT} Software Engineering Notes, {ACM} {SIGSOFT} Softw. Eng. Notes))dnl
define(JNL_SOSR, ifdef(`FULL', {ACM} {SIGOPS} Operating Systems Review, {SIGOPS} Oper. Syst. Rev.))dnl
define(JNL_STTT, ifdef(`FULL', International Journal on Software Tools for Technology Transfer, Int. J. Soft. Tool. Tech. Transf.))dnl
define(JNL_SYMM, ifdef(`FULL', Symmetry))dnl
define(JNL_TCOMPUT, ifdef(`FULL', IEEE Transactions on Computers, {IEEE} Trans. Computers))dnl
define(JNL_TCOMPUTS, ifdef(`FULL', ACM Transactions on Computer Systems, {ACM} Trans. Comput. Syst.))dnl
define(JNL_TCS, ifdef(`FULL', Theoretical Computer Science, Theor. Comput. Sci.))dnl
define(JNL_TINYTOCS, ifdef(`FULL', Tiny Transactions on Computer Science, Tin. Trans. Comput. Sci.))dnl
define(JNL_TOPLAS, ifdef(`FULL', {ACM} Transactions on Programming Languages and Systems, {ACM} Trans. Program. Lang. Syst.))dnl
define(JNL_TPDS, ifdef(`FULL', {IEEE} Transactions on Parallel and Distributed Systems, {IEEE} Trans. Parallel Distrib. Syst.))dnl
define(JNL_TN, ifdef(`FULL', {IEEE/ACM} Transactions on Networking, {IEEE/ACM} Trans. Netw.))dnl
define(JNL_TS, ifdef(`FULL', {ACM} Transactions on Storage, {ACM} Trans. Storage))dnl
define(JNL_TSE, ifdef(`FULL', {IEEE} Transactions on Software Engineering, {IEEE} Trans. Software Eng.))dnl
dnl
define(DAGSTUHL, {S}chloss {D}agstuhl--{Leibniz}-{Zentrum} f{\"u}r {I}nformatik)dnl
