#!/bin/sh

# Sorts biblio.m4 using bibtool.
#
# Usage:
#     sort.sh [output]
#
# NOTE: Overwrites biblio.m4 if no output is provided
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you can access it online at
# http://www.gnu.org/licenses/gpl-2.0.html.
#
# Author: Michalis Kokologiannakis <mixaskok@gmail.com>

# current directory
TOP="${TOP:-$PWD}"
OUT="${1:-$TOP/biblio.m4}"

# quit on error
set -e

# get current head before sorting (bibtool removes comments)
h=$(head -n 2 $TOP/biblio.m4)

# sort the input
sorted=$(tail --lines=+2 $TOP/biblio.m4 | bibtool -s -r $TOP/sort.rsc)

# overwrite exisiting file
echo "$h" > $OUT
echo "${sorted}" >> $OUT
