.SUFFIXES: .m4 .bib
.PHONY: default clean

export SHELL := /bin/bash

M4=m4
M4FLAGS=

INPUT=biblio.m4
OUTPUT=biblio.bib

default: $(OUTPUT)

biblio.bib: $(INPUT)
	$(M4) $(M4FLAGS) $(INPUT) > biblio.bib

clean:
	@$(RM) *.bib
	@$(RM) *~ \#*\#
