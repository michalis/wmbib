Weak Memory Bibliography
--------------------------------------------------------

This repository aims to be a useful collection of references to be
used in paper writing.

Maintained by: Michalis Kokologiannakis.

* [Dependencies](#deps)
* [Building the database](#build)
* [Editing the database](#edit)
* [Troubleshooting](#problems)

<a name="deps">Dependencies</a>
-------------------------------

Since December 2019, the bibliography database assumes `biblatex` is
used instead of `natbib`, and `bibtool` is necessary for sorting the
database

Some of the reasons why `biblatex` is preferred as well as useful
commands are listed [here](BIBLATEX.md).


<a name="build">Building the database</a>
-----------------------------------------

Issue `make` to produce the `.bib` file.

You can use long names for conferences/journals by setting
`M4FLAGS=-DFULL` when building the database.


<a name="edit">Editing the database</a>
--------------------------------------

### Styling

The `.m4` file is sorted alphabetically according to the citation
keys, braces are used for the field values, and the fields are sorted in
a particular fashion.

If you are about to insert a new bib item, please use the following
style:

* Conference/journal papers: `<name><year>:<keyword>`
* PhD theses: `<name>:thesis`
* Tools/web articles: `www:<keyword>`   (`@Misc` entry)
* Manuals/standards: `manual:<keyword>` (`@Book` entry)
* Books: `<book>:keyword`

### Inserting new items

In order to make sure that the new item adheres to the guidelines,
you can use `sort.sh`, provided that at least the key of the entry
is properly set, and the venue name uses the proper macro.

`sort.sh` uses `bibtool` to properly format entries.
